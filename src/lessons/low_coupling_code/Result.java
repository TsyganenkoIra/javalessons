package lessons.low_coupling_code;

public class Result {
    private One one;
    private Two two;

    public Result(One one, Two two) {
        this.one = one;
        this.two = two;
    }

    public void actionOne() {
        one.print();
    }

    public void actionTwo() {
        two.print();
    }
}

class NewResult {
    private Printable printable;

    protected NewResult(Printable printable) {
        this.printable = printable;
    }

    public void action() {
        printable.print();
    }

}

interface Printable {
    void print();
}

class One implements Printable {
    public void print() {
        System.out.println("One");
    }
}

class Two implements Printable {
    public void print() {
        System.out.println("Two");
    }
}

class TestCoupling {
    public static void main(String[] args) {
        One one = new One();

        Two two = new Two();

        NewResult newResult = new NewResult(two);


    }
}