package lessons.clonning;

public class LearnCloning {
    public static void main(String[] args) throws CloneNotSupportedException {
        Address address = new Address("Some");

        Original original = new Original("John", 23);

        original.setAddress(address);

        Original clone = original.clone();

        clone.getAddress().setName("Kiev");

//        clone.setName("Lviv");

        System.out.println(original.getName());

        System.out.println(original.getAddress().getName());

//        System.out.println(clone.getName());

    }
}

class Original implements Cloneable {
    private String name;
    private int age;
    private Address address;

    public Original(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public Original clone() throws CloneNotSupportedException {
        Original clone = (Original) super.clone();

        Address address = (Address) clone.getAddress().clone();

        clone.setAddress(address);

        return clone;
    }
}

class Address implements Cloneable {
    private String name;

    public Address(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
