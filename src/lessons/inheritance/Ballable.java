package lessons.inheritance;

public interface Ballable {

    void print();

    int COUNT = 12;

    static int sum() {
        return 5;
    }

    default void minus() {
        System.out.println();
    }

//    private int count() {
//
//    }
}


