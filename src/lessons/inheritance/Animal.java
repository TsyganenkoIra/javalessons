package lessons.inheritance;

class Animal {

}


/*
* private - visibility only inside class
* package - private - visibility only inside package
* protected - visibility only inside package + subclasses
* public - visibility anywhere
 * */