package lessons.generics;

import java.util.ArrayList;
import java.util.List;

public class LearnGeneric {

    public static <T> void print(T value) {
        System.out.println(value);
    }

    public static <T extends Comparable<T>> void print(T value1, T value2) {
        System.out.println(value1);
    }

    public static <T extends Number> void print2(T value1, T value2) {
        boolean equals = value1 == value2;
    }

    public static void main(String[] args) {
        NewMath<Integer, String> newMath = new NewMath<>();

        NewMath<Double, String> newMath1 = new NewMath<>();

        Object[] objects = new Object[2];

        objects[0] = Integer.valueOf(2);
        objects[1] = "Hello";

        List<Integer> list = new ArrayList();

        LearnGeneric.print2(3, .5);

    }
}

class MathInteger {
    private int[] array;
}

class MathDouble {
    private double[] array;
}

class NewMath<T, F> {
    T[] array;
    F age;

    public T[] getArray() {
        return array;
    }

    public void setArray(T[] array) {
        this.array = array;
    }

    public F getAge() {
        return age;
    }

    public void setAge(F age) {
        this.age = age;
    }
}
