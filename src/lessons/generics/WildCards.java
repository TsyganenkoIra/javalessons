package lessons.generics;

import java.util.Arrays;
import java.util.List;

public class WildCards {

    public static void print(List<? extends Number> numbers) {//upper wildcard
        System.out.println(numbers);
    }

    public static void print1(List<? super Number> numbers) {//lower wildcard
        System.out.println(numbers);
    }

    public static void print2(List numbers) {
        System.out.println(numbers);
    }

    public static void main(String[] args) {

        List<Integer> integers = Arrays.asList(1, 2, 3);

        print2(integers);
    }
}
