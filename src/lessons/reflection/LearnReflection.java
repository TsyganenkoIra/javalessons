package lessons.reflection;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class LearnReflection {
    public static void main(String[] args) {
        ManRef manRef = new ManRef(23, "John");

        Class<? extends ManRef> aClass = manRef.getClass();

//        System.out.println(aClass.getSimpleName());

        Class<?> superClass = aClass.getSuperclass();

//        System.out.println(superClass.getSimpleName());

        int modifier = aClass.getModifiers();

        if (!Modifier.isPublic(modifier)) {
//            System.out.println("Package-private");
        }

        Class<?>[] interfaces = aClass.getInterfaces();

        for (Class<?> anInterface : interfaces) {
//            System.out.println(anInterface.getSimpleName());
        }

        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            Class<?> type = field.getType();
            System.out.println(field.getName() + "\t" + type.getSimpleName() + "\t" + Modifier.isPublic(type.getModifiers()));
        }
    }
}


class ManRef extends File implements Serializable, Cloneable {
    private int age;
    private String name;

    public ManRef(int age, String name) {
        super("");
        this.age = age;
        this.name = name;
    }
}