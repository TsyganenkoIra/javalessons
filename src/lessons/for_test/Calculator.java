package lessons.for_test;

import java.util.Random;

public final class Calculator {
    public int sum(int v1, int v2) {
        return v1 + v2;
    }

    public int newSum(int v1) {
        return v1 + getRandomValue();
    }

    private int getRandomValue() {
        return new Random().nextInt(20);
    }

    public static int getSomeValue() {
        return new Random().nextInt(20);
    }

    public final int getValueFinal() {
        return new Random().nextInt(20);
    }

}