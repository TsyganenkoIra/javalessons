package lessons.regex;

import java.util.regex.Pattern;

public class LearnRegex {
    public static void main(String[] args) {
        /*String regex = "H.llo";//any symbol

        String text = "Hqllo";
        */
        /*String regex = "H[ea]llo";//or e or a symbol

        String text = "Hqllo";*/

        String regex = "H[a-r]llo";//or e or a symbol

        String text = "Hqllo";

        System.out.println(Pattern.matches(regex, text));
    }
}
