package lessons.collection;

import java.util.HashMap;
import java.util.Map;

public class LearnMap {
    public static void main(String[] args) {
        Map<Man, Integer> map = new HashMap<>();
        /*map.put("One", 1);

        map.put("Two", 2);*/

        Man john = new Man("John", 12);
        Man john1 = new Man("John", 12);

//        System.out.println(john.hashCode());
//        System.out.println(john1.hashCode());

//        map.put(null, null);

        map.put(john, 1);
        map.put(john1, 2);

        /*
        * hashCode is overriden, equals not overriden - added 2 objects
        * hashCode not overriden, equals overriden - added 2 objects
        * hashCode overriden, equals overriden - added 1 object
        * hashCode always return const value, equals overriden - added 1 object
        * */

        System.out.println(map);

//        System.out.println(map.get(null));

    }
}
