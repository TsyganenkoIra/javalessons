package lessons.collection.functional_interface;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;

public class LearnFI {

    public static void calc(Calculatable calculatable) {
        calculatable.sum(5, 8);
    }

    public static void main(String[] args) {
        Calculatable calculatable = (v1, v2) -> v1 + v2;

        calc((v1, v2) -> v1 * v2);

//        System.out.println(calculatable.sum(5, 8));

//        Convertable<String, Integer> convertable = (v1) -> Integer.parseInt(v1);
        Convertable<String, Integer> convertable = Integer::parseInt;

//        System.out.println(convertable.convert("45"));

        Consumer<String> consumer = System.out::println;

        consumer.accept("Hello");

        Supplier<String> supplier = () -> "Hello";

        System.out.println(supplier.get());
    }
}

@FunctionalInterface
interface Calculatable {
    int sum(int v1, int v2);

    default void print() {

    }
}

@FunctionalInterface
interface Convertable<T, F> {
    F convert(T v1);

    default void print() {

    }
}