package lessons.collection.lambdas;

import lessons.collection.Child;
import lessons.collection.Man;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LearnLambdas {
    public static void main(String[] args) {
        List<Man> men = Arrays.asList(
                new Man("John", 21),
                new Man("Jack", 54),
                new Man("Susan", 4),
                new Man("Steefe", 5)
        );

       // Predicate<Man> predicate = m -> m.getAge() >= 54;

//        List<Man> men1 = men.stream().filter(predicate).collect(Collectors.toList());

       System.out.println(men.stream().anyMatch(m -> m.getAge() > 20));
//        System.out.println(men.stream().allMatch(m -> m.getAge() > 20));

    men.forEach(System.out::println);

        for (int i = 0; i < 100; i++) {

        }

//        IntStream.range(0, 100).filter(index -> index % 2 == 0).forEach(index -> );

        Function<Man, Child> manChildFunction = man -> new Child(man.getName(), man.getAge());

//        List<Child> children = men.stream().map(manChildFunction).collect(Collectors.toMap());

        int sum = men.stream().mapToInt(Man::getAge).sum();

//        System.out.println(sum);

        Map<Integer, List<Man>> map = men.stream().collect(Collectors.groupingBy(m -> m.getAge() % 3));

//        System.out.println(map.get(0));

        //men.stream().sorted().collect(Collectors.toList())
        Comparator<Man> comparator = (m1, m2) -> Integer.compare(m1.getAge(), m2.getAge());

        Comparator<Man> comparator1 = Comparator.comparingInt(Man::getAge);

        Collections.sort(men, comparator.reversed());

    }
}
