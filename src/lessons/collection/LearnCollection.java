package lessons.collection;

import java.util.*;

public class LearnCollection {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();

        integers.add(2);
        integers.add(5);
//FIFO, LIFO
//        System.out.println(integers.size());

//        new PriorityQueue<>().add(null);

        Set<Integer> set = new TreeSet<>();

        set.add(5);
        set.add(5);
        set.add(2);
        set.add(4);

//        System.out.println(set);

        Comparator<Man> comparator = new Comparator<Man>() {
            @Override
            public int compare(Man o1, Man o2) {
                return Integer.compare(o1.getAge(), o2.getAge());
            }
        };

        Set<Man> men = new TreeSet<>(comparator.reversed());

        men.add(new Man("John", 23));

        for (int i = 0; i < integers.size(); i++) {
            integers.get(i);
        }

        for (Integer integer : integers) {

        }

        Iterator<Integer> iterator = integers.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}

