package lessons.collection;

public class Man implements Comparable<Man> {
    private String name;

    private int age;

    public Man(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Man o) {
        return Integer.compare(this.age, o.getAge());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Man man = (Man) o;

        if (age != man.age) return false;
        return name.equals(man.name);
    }

    @Override
    public int hashCode() {
//        return 31 * name.hashCode() + age;
        return 31;
    }

    @Override
    public String toString() {
        return "Man{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

