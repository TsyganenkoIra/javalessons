package lessons;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleTry {
    public static void main(String[] args) {
        String s = "1011001101100000110000100";

        Pattern pattern = Pattern.compile("10+1");

        List<String> strings = new ArrayList<>();

        Matcher matcher = pattern.matcher(s);

        StringBuilder builder = new StringBuilder();

        while (matcher.find()) {
            builder.append(matcher.group());
        }

        Pattern pattern1 = Pattern.compile("0+");

        Matcher matcher1 = pattern1.matcher(builder.toString());

        while (matcher1.find()) {
            strings.add(matcher1.group());
        }

        System.out.println(strings.stream().max(Comparator.comparingInt(String::length)).get().length());

    }
}
