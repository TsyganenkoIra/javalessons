package lessons.exceptions;

import java.io.IOException;

public class LearnException {

    public static void throwException() throws IraException {
//        throw new NullPointerException("I'm NPE");
        throw new IraException("Hello");
    }

    public static void main(String[] args)  {
        try {
            throwException();
        } catch (IraException e) {
            e.handleException();
        }
    }
}

class IraException extends Exception {
    public IraException(String message) {
        super(message);
    }

    public void handleException() {
        System.out.println("Our exception");
    }
}
