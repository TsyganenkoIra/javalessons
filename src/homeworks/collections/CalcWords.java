package homeworks.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalcWords {

    private Map<String, Integer> words;

    public CalcWords() {
        words = new HashMap<>();
    }

    public void calculateWords(String sentence) {

        Pattern pattern = Pattern.compile("[a-zA-Z]+");
        Matcher matcher = pattern.matcher(sentence);

       /* while (matcher.find()) {
            String s = matcher.group();
            if (words.containsKey(s)) {

                words.put(s, words.get(s) + 1);
            } else {
                words.put(s, 1);
            }


        }*/

        while (matcher.find()) {
            String s = matcher.group();

            int result = 1;

            if (words.containsKey(s)) {

                result = words.get(s) + 1;
            }

            words.put(s, result);
        }

    }


    public Map<String, Integer> getWords() {
        return words;
    }
}
