
package homeworks.internet_shop;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {

    private static final Scanner SCANNER = new Scanner(new InputStreamReader(System.in));;

    public static void main(String[] args) throws IOException {

        System.out.println("New store =>" + "\nInput Name of store, please: ");

        String name = "";

        do {
            name = SCANNER.nextLine();
        } while (!checkName(name));
        Store store1 = new Store(name);

        System.out.println("*********Add good 1 =>**********"
                + "\nInput Name of good, please: ");

        do {
            name = SCANNER.nextLine();
        } while (!checkName(name));

        System.out.println("Input price of good, please: ");
        String number ="";
        do{
            number = SCANNER.nextLine();
        }while(!checkDouble(number));
        double price = Double.parseDouble(number);

        System.out.println("Input Name of Producer, please: ");
        String producer ="";
        do {
            producer = SCANNER.nextLine();
        } while (!checkName(producer));

       System.out.println("Input the Production Date, please: ");
       String date = "";

       do{
           System.out.println("Format: dd.mm.yyyy");
           date = SCANNER.nextLine();
       }while(!checkDate(date));
        Date prDate = convertFromStringToDate(date);

     Good good1 = new Good(name, price, producer, prDate, Section.COMPUTERS_ELECTRONICS, Subsection.TABLETS);
     store1.addGood(good1);

        System.out.println("*********Add good 2 =>**********"
                + "\nInput Name of good, please: ");

        do {
            name = SCANNER.nextLine();
        } while (!checkName(name));

        System.out.println("Input price of good, please: ");
        do{
            number = SCANNER.nextLine();
        }while(!checkDouble(number));
        price = Double.parseDouble(number);

        System.out.println("Input Name of Producer, please: ");
        do {
            producer = SCANNER.nextLine();
        } while (!checkName(producer));

        System.out.println("Input the Production Date, please: ");
        do{
            System.out.println("Format: dd.mm.yyyy");
            date = SCANNER.nextLine();
        }while(!checkDate(date));
       prDate = convertFromStringToDate(date);

        Good good2 = new Good(name, price, producer, prDate, Section.CONSUMER_ELECTRONICS, Subsection.SMALL_KITCHEN_APP);
        store1.addGood(good2);

        System.out.println("*********Add good 3 =>**********"
                + "\nInput Name of good, please: ");

        do {
            name = SCANNER.nextLine();
        } while (!checkName(name));

        System.out.println("Input price of good, please: ");
        do{
            number = SCANNER.nextLine();
        }while(!checkDouble(number));
        price = Double.parseDouble(number);

        System.out.println("Input Name of Producer, please: ");
        do {
            producer = SCANNER.nextLine();
        } while (!checkName(producer));

        System.out.println("Input the Production Date, please: ");
        do{
            System.out.println("Format: dd.mm.yyyy");
            date = SCANNER.nextLine();
        }while(!checkDate(date));
        prDate = convertFromStringToDate(date);

        Good good3 = new Good(name, price, producer, prDate, Section.CONSUMER_ELECTRONICS, Subsection.VACUUMS_FLORCARE);
        store1.addGood(good3);

       System.out.println("************Show all goods =>**************");
        store1.showAllGoods();

        System.out.println("*******Delete good 1********");
        store1.deleteGood(good2);

        store1.showAllGoods();


         System.out.println("*******Show all goods in Section COMPUTERS_ELECTRONICS:**********");
        store1.showGoodsBySection(Section.COMPUTERS_ELECTRONICS);

        System.out.println("*********New client+" +
                 "/nInput the Name, please: ");

        do {
            name = SCANNER.nextLine();
        } while (!checkName(name));

        System.out.println("Input the Date Of Birth(in the format: dd.MM.yyyy), please: ");

        do{
            System.out.println("Format: dd.mm.yyyy");
            date = SCANNER.nextLine();
        }while(!checkDate(date));
        prDate = convertFromStringToDate(date);

        Client client1 = new Client(name, prDate);


        System.out.println("*********Add goods to the bag*******");
        client1.getShoppingBag().addGood(good1, 3);

        System.out.println("**************Calculate sum of the bag****************");
       client1.getShoppingBag().calculateSum();

    }
    public static boolean checkDate(String dateStr) {
        boolean flag = false;

        if (!dateStr.isEmpty()) {
            Pattern p = Pattern.compile("^(((0[1-9]|[12]\\d|3[01])\\.(0[13578]|1[02])" +
                    "\\.((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\.(0[13456789]|1[012])" +
                    "\\.((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\.02\\.((19|[2-9]\\d)\\d{2}))" +
                    "|(29\\.02\\.((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");
            Matcher m = p.matcher(dateStr);
            flag = m.matches();
        } else{
            System.out.println("The Date should not be empty! Try again!");
        }
        return flag;

    }

    public static Date convertFromStringToDate(String dateStr) {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        try {
            date = format.parse(dateStr);
             } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static boolean checkName(String name) {
        boolean flag = false;

        if (!name.isEmpty()) {
            flag = true;
        } else {
            System.out.println("Incorrect name format! The name should not be empty! Try again!");
        }
        return flag;
    }

    public static boolean checkDouble(String doubleNumber) throws IOException {
        boolean flag = false;

        if (!doubleNumber.isEmpty()) {
            try {
                Double number = Double.parseDouble(doubleNumber);
                flag = true;
            } catch (NumberFormatException e) {
                System.out.println("Incorrect number format! Try again!");
            }
        } else{
            System.out.println("The price should not be empty! Try again!");
        }
        return flag;
      }
}

