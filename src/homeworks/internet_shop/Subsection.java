package homeworks.internet_shop;

public enum Subsection {
    SMALL_KITCHEN_APP, VACUUMS_FLORCARE, LAPTOPS, TABLETS;
}
