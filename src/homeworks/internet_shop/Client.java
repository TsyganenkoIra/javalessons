package homeworks.internet_shop;

import java.util.Date;

public class Client {
	private String name;
	private Date dateOfBirth;//LocalDate
	private Bag shoppingBag;


	public Client(String name, Date date) {
		this.name = name;
		dateOfBirth = date;
		shoppingBag = new Bag();
	}

	public String getName() {
		return name;
	}

	public Bag getShoppingBag() {
		return shoppingBag;
	}

}
