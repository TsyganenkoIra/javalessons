
package homeworks.internet_shop;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Bag {

    private Map<Good, Integer> orders;

    public Bag() {
        orders = new HashMap<>();

    }

    public void addGood(Good good, int qty) {

        orders.put(good, qty);
    }

    public void deleteGood(Good good) {
        orders.remove(good);
    }

    public void calculateSum() {
        Calculatable calc = (x, y) -> x * y;

        double sum = 0;

        for (Entry<Good, Integer> entry : orders.entrySet()) {

            Good key = entry.getKey();

            double price = key.getPrice();

            int qty = entry.getValue();

            sum += calc.calculate(price, qty);

            System.out.println("Good: " + key.getName() +
                    ", price: " + price +
                    ", qty: " + qty + ";");
        }

        System.out.println("Total sum = " + sum + "$");

    }

}

@FunctionalInterface
interface Calculatable {
    double calculate(double x, int y);
}

