
package homeworks.internet_shop;

import java.util.Date;

public class Good {

    private String name;
    private double price;
    private String producer;
    private Date productionDate;//LocalDate
    private Section section;
    private Subsection subsection;


    public Good(String name, double price, String producer,
                Date productionDate, Section section, Subsection subsection) {
        this.name = name;
        this.price = price;
        this.producer = producer;
        this.productionDate = productionDate;
        this.section = section;
        this.subsection = subsection;
    }

    @Override
    public String toString() {
        return "Good : name=" + name +
                ", price=" + price +
                ", producer=" + producer + ", productionDate="
                + productionDate + ", subsection= " + subsection.name();
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getSection() {
        return section.name();
    }

}

