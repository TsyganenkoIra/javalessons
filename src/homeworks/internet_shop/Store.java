package homeworks.internet_shop;

import java.util.ArrayList;
import java.util.List;

public class Store {
    private String name;

    private List<Client> clients;

    private List<Good> goods;


    public Store(String storeName) {

        name = storeName;

        goods = new ArrayList<>();

        clients = new ArrayList<>();

    }

    public String getName() {
        return name;
    }


    public void addClient(Client client) {

        clients.add(client);
    }

    public void deleteClient(Client client) {

        clients.remove(client);
    }

    public void addGood(Good good) {
        goods.add(good);
    }


    public void deleteGood(Good good) {

        goods.remove(good);
    }


    public void showAllGoods(){
        for (Good good : goods) {
            System.out.println(good);
        }
    }

    public void showGoodsBySection(Section section) {
        for (Good good : goods) {
            if (good.getSection().equals(section.name())) {
                System.out.println(good);
            }
        }
    }

    public List<Good> getGoods() {

        return goods;
    }
}
