package homeworks.messenger;

import java.nio.file.Path;
import java.time.LocalDateTime;


public class Message {
    private User from;
    private User to;
    private String text;
    private LocalDateTime date;
    private Path path;

    public Message(User from, User to, LocalDateTime date, String text, Path path) {
        this.from = from;
        this.to = to;
        this.text = text;
        this.date =date;
        this.path = path;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Path getPath() {
        return path;
    }
}

