package homeworks.messenger;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class Messenger {

    private Set<User> users;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(" dd MMM HH:mm ");

    public Messenger() {
        users = new HashSet<>();
    }

    public void sendSMS(User from, User to, String text, Path path) {

        if (to.getStatus() == Status.OFFLINE) {
            System.out.println(to.getName() + " is OFFLINE");
            return;
        }

        LocalDateTime time = LocalDateTime.now();

        String dataTime = time.format(FORMATTER);

        String textToChat = from.getName() + " " + dataTime + ": " + text;

        from.addSMS(new Message(null, to, time, textToChat, null));

        to.addSMS(new Message(from, null, time, textToChat, path));

        if (Objects.nonNull(path)) {
            to.addFile(path);
        }

    }


    public void showPeriodOfHistory(User user, LocalDateTime from, LocalDateTime to) {

        user.getMessages().forEach((m) -> {
            if (m.getDate().isAfter(from) && m.getDate().isBefore(to))
                System.out.println(m.getText());
        });

        user.getMessages()
                .stream()
                .filter(m -> m.getDate().isAfter(from) && m.getDate().isBefore(to))
                .forEach(m -> System.out.println(m.getText()));

    }

    public void showWholeHistory(User user) {
        user.getMessages().forEach((m) -> System.out.println(m.getText()));
    }

    public void registerUser(User user) {
        users.add(user);
    }

    public Set<User> getUsers() {
        return users;
    }

}
