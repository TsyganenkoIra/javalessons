package homeworks.messenger;

import java.nio.file.Path;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private LocalDate dateOfBirth;
    private Status status;
    private List<Message> messages;
    private List<Path> files;

    public User(String name, LocalDate dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        status = Status.OFFLINE;
        messages = new ArrayList<>();
        files = new ArrayList<>();
    }

    public void addSMS(Message message) {
        messages.add(message);
    }

    public void addFile(Path path) {
        files.add(path);
    }

    public List<Path> getFiles() {
        return files;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        Period period = Period.between(dateOfBirth, LocalDate.now());
        return period.getYears();
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!getName().equals(user.getName())) return false;
        return dateOfBirth.equals(user.dateOfBirth);
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + dateOfBirth.hashCode();
        return result;
    }
}
