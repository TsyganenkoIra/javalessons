package homeworks.messenger;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class App {

    /*Приложение должно позволять:
         + - писать сообщение определенному зарегистрированному пользователю и если он имеет статус "В сети".
         + - сохранять историю переписки.
          - выводить историю(за весь период и за определенный период).
         + - выводить личную информацию о пользователе(имя, фамилия, возраст).
          - смена автора.
         + - регистрироваться пользователю.
          - отправлять файлы.
          */
    public static void main(String[] args) {

        Messenger messenger = new Messenger();

        User user1 = new User("User1", LocalDate.of(1990, 10, 10));
        User user2 = new User("User2", LocalDate.of(1999, 10, 19));
        User user3 = new User("User3", LocalDate.of(1989, 11, 10));

        messenger.registerUser(user1);
        messenger.registerUser(user2);
        messenger.registerUser(user3);

        user1.setStatus(Status.ONLINE);
        user2.setStatus(Status.ONLINE);
        user3.setStatus(Status.ONLINE);

        messenger.sendSMS(user1, user2, "First SMS from 1->2", null );
        messenger.sendSMS(user2, user1, "First answer SMS 2->1", null );
        messenger.sendSMS(user2, user3, "First SMS 2->3", null );
        messenger.sendSMS(user1, user2, "Second SMS 1->2", null );

        messenger.showWholeHistory(user3);

        LocalDateTime from = LocalDateTime.of(2018,8, 29, 0, 0);

        LocalDateTime to = LocalDateTime.of(2018,8, 31, 0, 0);

        messenger.showPeriodOfHistory(user3, from, to);
    }
}
