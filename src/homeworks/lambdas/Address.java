package homeworks.lambdas;

public class Address {
    private String country;
    private String city;
    private String street;
    private int numberOfHome;
    private int quantityOfCitizens;

    public Address(String country, String city, String street, int numberOfHome, int quantityOfCitizens) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.numberOfHome = numberOfHome;
        this.quantityOfCitizens = quantityOfCitizens;
    }

    public Address() {
    }

    @Override
    public String toString() {
        return " Address: " +
                country + ", " +
                city + " (" + quantityOfCitizens + "), " +
                street + "," +
                numberOfHome + ". ";
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getNumberOfHome() {
        return numberOfHome;
    }

    public int getQuantityOfCitizens() {
        return quantityOfCitizens;
    }

}
