package homeworks.lambdas;

public class Man {
    private String firstName;
    private String lastName;
    private int age;
    private int quantityOfChildren;
    private Address address;

    public Man(String firstName, String lastName, int age, int quantityOfChildren,
               String country, String city, String street, int numberOfHome, int quantityOfCitizens) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.quantityOfChildren = quantityOfChildren;
        this.address = new Address(country, city, street, numberOfHome, quantityOfCitizens);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public int getQuantityOfChildren() {
        return quantityOfChildren;
    }

    public Address getAddress() {
        return address;
    }

    public String getCountry() {
        return getAddress().getCountry();
    }

    public String getCity() {
        return getAddress().getCity();
    }

    public String getStreet() {
        return getAddress().getStreet();
    }

    public int getNmberOfHome() {
        return getAddress().getNumberOfHome();
    }

    public int getQuantityOfCitizens() {
        return getAddress().getQuantityOfCitizens();
    }

    @Override
    public String toString() {
        return firstName +
                " " + lastName +
                ", " + age + " yo, " +
                +quantityOfChildren + " children."
                + address;
    }
}