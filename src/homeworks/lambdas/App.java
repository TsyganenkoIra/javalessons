package homeworks.lambdas;

import java.util.*;
import java.util.stream.Collectors;

public class App {
 /*
    public static void main(String[] args) {

      List<Man> world = new ArrayList<>();

        Man man1 = new Man("Ivan", "Dorn", 25, 2,
                "RF", "Moscow", "Lenina", 4, 10000000);
        world.add(man1);

        Man man2 = new Man("Pavel", "Volya", 35, 1,
                "RF", "Piter", "Lomomosova", 85, 1500000);

        world.add(man2);

        Man man3 = new Man("Tonya", "Dolina", 15, 0,
                "UA", "Kyiv", "Kylikova", 70, 2000000);
        world.add(man3);
        Man man4 = new Man("Lena", "Pervaya", 22, 3,
                "LT", "Vilnus", "Pronina", 155, 800000);
        world.add(man4);
        Man man5 = new Man("John", "Kennedi", 55, 4,
                "US", "Vashington", "5TH Ave", 899, 10000000);
        world.add(man5);
        Man man6 = new Man("Kris", "Torn", 30, 2,
                "Canada", "Shelbrook", "Sterling Ave", 3, 50000);
        world.add(man6);
        Man man7 = new Man("Lev", "Durov", 37, 2,
                "Greece", "Athens", "Borova", 59, 4000000);
        world.add(man7);
        Man man8 = new Man("Sveta", "Aseeva", 32, 1,
                "UA", "Kharkiv", "Lugova", 16, 2000000);
        world.add(man8);
        Man man9 = new Man("Roman", "Zaez", 10, 0,
                "UA", "Lviv", "Lenina", 55, 1000000);
        world.add(man9);
        Man man10 = new Man("Olga", "Kolik", 20, 2,
                "US", "LA", "1TH Ave", 263, 5000000);
        world.add(man10);

        //  "SELECT * FROM Man";
//         world.forEach(System.out::println);

        //"SELECT * FROM Address";
        world.forEach(m -> System.out.println(m.getAddress()));

//        world.stream().map(Man::getAddress).forEach(System.out::println);

        //"SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";
        world.stream().filter((man -> man.getAge() >= 20)).sorted(Comparator.comparing(Man::getFirstName)).forEach(System.out::println);

        //"SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";
        world.stream().filter((man -> man.getCountry().equals("Canada") && man.getNmberOfHome() == 3)).
                forEach(man -> System.out.println(man.getFirstName() + " " + man.getLastName() + " " + man.getStreet()));
        //"SELECT count(*) FROM Man GROUP BY countOfChildren"
        Map<Integer, Long> map = world.stream().collect(Collectors.groupingBy(Man::getQuantityOfChildren, Collectors.counting()));

        System.out.println(map.get(4));
        System.out.println(map.get(3));
        System.out.println(map.get(2));
        System.out.println(map.get(1));
        System.out.println(map.get(0));

        //"SELECT count(*) FROM Man GROUP BY countOfChildren, age"
//         world.stream().collect(Collectors.groupingBy(Man::getQuantityOfChildren));

        class Order<T extends Comparable<T>, V extends Comparable<V>> {
            private int age;
            private int childs;

            public Order(int age, int childs) {
                this.age = age;
                this.childs = childs;
            }
        }

    *//*    Map<AbstractMap.SimpleEntry, Long> collect = world.stream().collect(Collectors.groupingBy(man ->
                new AbstractMap.SimpleEntry(man.getQuantityOfChildren(), man.getAge()), Collectors.counting()));*//*

        Map<Order, Long> collect = world.stream().collect(Collectors.groupingBy(man ->
                new Order(man.getQuantityOfChildren(), man.getAge()), Collectors.counting()));

        //"SELECT count(*) FROM Address GROUP BY city, nameOfStreet";

        //"SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 4";

        long count = world.stream()
                .map(Man::getAddress)
                .collect(Collectors.groupingBy(a ->
                        new AbstractMap.SimpleEntry(a.getCity(), a.getStreet()), Collectors.counting())).
                        entrySet()
                .stream()
                .filter(e -> e.getValue() > 4)
                .count();

        Map<AbstractMap.SimpleEntry, Long> longMap = world.stream().map(Man::getAddress).collect(Collectors.groupingBy(a ->
                new AbstractMap.SimpleEntry(a.getCity(), a.getStreet()), Collectors.counting()));

        Set<Map.Entry<AbstractMap.SimpleEntry, Long>> entries = longMap.entrySet();


        // "SELECT count(*) FROM Man GROUP BY city, nameOfStreet";
        world.stream().collect(Collectors.groupingBy(Man::getCity));
       *//*

       "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 4";

      "UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country)
*//*
    }
*/
}
