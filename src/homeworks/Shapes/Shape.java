package homeworks.Shapes;

public abstract class Shape {

    private String name;
    private Point centre;

    public Shape(String name, double x, double y) {
        this.name = name;
        this.centre = new Point(x, y);
    }

    abstract public void drawShape();

}
