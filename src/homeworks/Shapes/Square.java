package homeworks.Shapes;

public class Square extends Shape{

    private double length;

    Square(String name, double x, double y, double length){
        super(name, x, y);
        this.length = length;
    }

    @Override
    public void drawShape() {
    System.out.println("Drawing Square.");
    }
}
