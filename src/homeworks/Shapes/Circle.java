package homeworks.Shapes;

public class Circle extends Shape {

    private double 	radius;

    Circle(String name, double x, double y, double radius){
        super(name, x, y);
        this.radius = radius;
    }
    @Override
    public void drawShape() {
        System.out.println("Drawing Circle.");
    }
}
