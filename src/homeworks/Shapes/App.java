package homeworks.Shapes;

public class App {
    public static void main(String [] args){

        Shape square = new Square("Square1", 5, 10, 8);
        Shape circle = new Circle("Circle1", 7, 17, 10);

        square.drawShape();
        circle.drawShape();
    }
}
