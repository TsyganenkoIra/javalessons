package homeworks.file_manager;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.log.SysoCounter;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;


public class FileManager {

    private static final String PDF_EXTENSION = ".pdf";

    public void createFile(Path path) {

        if (checkPathExistence(path)) {
            System.out.println("File already exsists");
            return;
        }

        try {
            Files.createFile(path);
            System.out.println("File created successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createDirectory(Path path) {

        if (checkPathExistence(path)) {
            System.out.println("Directory already exsists");
            return;
        }

        try {
            Files.createDirectory(path);
            System.out.println("Directory created successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void convertToPDF(Path sourcePath) throws IOException {

        if (!checkPathExistence(sourcePath)) {
            System.out.println("Source does not exsist!");
            return;
        }

        String text = new String(Files.readAllBytes(sourcePath));

        String pathPDF = sourcePath + PDF_EXTENSION;

        if (text.isEmpty()) {
            Path p = Paths.get(pathPDF);
            createFile(p);
            return;
        }

        Document document = new Document();

        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pathPDF));

            document.open();
            document.add(new Paragraph(text));
            document.close();
            writer.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void copyFilesToOtherDirectory(Path pathSource, Path target) throws IOException {

        if (!checkPathExistence(pathSource)) {
            System.out.println("Source does not exist");
            return;
        }

        createDirectory(target);

        Files.walkFileTree(pathSource, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
                new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        String fileName = target + "/" + file.getFileName().toString();

                        Path pathDestination = Paths.get(fileName);

                        Files.copy(file, pathDestination, StandardCopyOption.REPLACE_EXISTING);

                        return FileVisitResult.CONTINUE;
                    }
                });

    }

    public boolean checkPathExistence(Path path) {

        return Files.exists(path, LinkOption.NOFOLLOW_LINKS);
    }

    public void deleteFile(Path pathSource) throws IOException {

        if (Files.isDirectory(pathSource)) {
            System.out.println("The path is directory! Please, use method deleteDirectory!");
            return;
        }

        Files.deleteIfExists(pathSource);

    }

    public void deleteDirectory(Path pathSource) throws IOException {

        if (!Files.isDirectory(pathSource)) {
            System.out.println("The path isn`t directory! Please, use method deleteFile!");
            return;
        }

        Files.walkFileTree(pathSource, EnumSet.of(FileVisitOption.FOLLOW_LINKS),
                Integer.MAX_VALUE, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.deleteIfExists(file);
                        return FileVisitResult.CONTINUE;
                    }
                });

        Files.delete(pathSource);
    }

    public void lookThroughDirectory(Path pathSource) throws IOException {
        if (!Files.exists(pathSource)) {
            System.out.println("The directory doesn`t excist!");
            return;
        }

        if (!Files.isDirectory(pathSource)) {
            System.out.println("The path isn`t directory!");
            return;
        }

        Files.walkFileTree(pathSource, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
                new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        System.out.println(file.getFileName().toString());
                        return FileVisitResult.CONTINUE;
                    }
                });
    }


    public void renameFile(String oldName, String newName) throws IOException {
        Path pathSource = Paths.get(oldName);
        if (!checkPathExistence(pathSource)) {
            System.out.println("Source does not exsist!");
            return;
        }
        Files.move(pathSource, pathSource.resolveSibling(newName));
    }

    public void renameDirectory(Path pathSource, Path newDirection) throws IOException {

        if (!checkPathExistence(pathSource)) {
            System.out.println("Source does not exsist!");
            return;
        }


        if (!checkPathExistence(newDirection)) {
            createDirectory(newDirection);
        }

        Files.walkFileTree(pathSource, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
                new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.move(file, newDirection.resolve(file.getFileName()),
                                StandardCopyOption.REPLACE_EXISTING);
                        return FileVisitResult.CONTINUE;
                    }
                });

        Files.delete(pathSource);
    }

}