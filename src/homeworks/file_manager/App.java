package homeworks.file_manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {

    public static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    private final static String MY_PATH_FOR_FILE_MANAGER = "../IdeaProjects/ForFileManager/";
    private final static String MY_PATH_TO_FILE_MANAGER = "../IdeaProjects/ToFileManager";
    private final static String RENAMED_DIRECTORY = "../IdeaProjects/RenamedDirectory";

    public static void main(String[] args) {

        FileManager manager = new FileManager();

        boolean mark = Boolean.TRUE;

        do {
            System.out.println("Choose action, please:"
                    + "\n1: Create text file"
                    + "\n2: Create directory"
                    + "\n3: Convertation to PDF"
                    + "\n4: Copying file from one directory to other"
                    + "\n5: Deletion file"
                    + "\n6: Deletion directory"
                    + "\n7: Rename file"
                    + "\n8: Rename directoty"
                    + "\n9: Look throught content of directory"
                    + "\n0: Exit");

            try {
                String action = READER.readLine();

                switch (action) {
                    case "0":
                        mark = false;
                        break;

                    case "1":
                        Path path1 = Paths.get(MY_PATH_FOR_FILE_MANAGER + "test.txt");
                        manager.createFile(path1);
                        Path path2 = Paths.get(MY_PATH_FOR_FILE_MANAGER + "test2.txt");
                        manager.createFile(path2);

                        /*
                        * move to constant and use functions of Path class
                        * */

                        break;

                    case "2":
                        Path pathFor = Paths.get(MY_PATH_FOR_FILE_MANAGER);
                        manager.createDirectory(pathFor);
                        Path pathTo = Paths.get(MY_PATH_TO_FILE_MANAGER);
                        manager.createDirectory(pathTo);
                        break;

                    case "3":
                        Path sourcePath = Paths.get(MY_PATH_FOR_FILE_MANAGER + "test.txt");
                        manager.convertToPDF(sourcePath);
                        break;

                    case "4":
                        Path pathSource = Paths.get(MY_PATH_FOR_FILE_MANAGER);
                        Path target = Paths.get(MY_PATH_TO_FILE_MANAGER);
                        manager.copyFilesToOtherDirectory(pathSource,
                                                          target);
                        break;

                    case "5":
                        Path pathSource1 = Paths.get(MY_PATH_FOR_FILE_MANAGER + "test.txt");
                        manager.deleteFile(pathSource1);
                        break;

                    case "6":
                        Path pathSource2 = Paths.get(MY_PATH_FOR_FILE_MANAGER);
                        manager.deleteDirectory(pathSource2);
                        break;

                    case "7":
                        manager.renameFile(MY_PATH_FOR_FILE_MANAGER + "test.txt",
                                           "renamedFile.txt");
                        break;

                    case "8":
                        Path pathSource3 = Paths.get(MY_PATH_FOR_FILE_MANAGER);
                        Path newDirection = Paths.get(RENAMED_DIRECTORY);
                        manager.renameDirectory(pathSource3,
                                newDirection);
                        break;

                    case "9":
                        Path pathSource4 = Paths.get(MY_PATH_FOR_FILE_MANAGER);
                        manager.lookThroughDirectory(pathSource4);
                        break;

                    default:
                        System.out.println("Unknown action");
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Something went wrong!");
            }
        }
        while (mark);
    }
}