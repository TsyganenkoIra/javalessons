package homeworks.GeometricFigures;

public interface Calculatable {

    void calculateSquare();

    default void print() {
        System.out.println(this.getClass().getSimpleName());
    }
}
