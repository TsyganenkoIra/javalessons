package homeworks.GeometricFigures;

public class Triangle extends Figure {

    Triangle(double length, double height) {
        super(length * 0.5, height, "Triangle");
    }

}
