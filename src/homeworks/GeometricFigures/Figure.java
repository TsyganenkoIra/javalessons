package homeworks.GeometricFigures;

abstract class Figure implements Calculatable {

    private double length;
    private double height;

    public Figure(double length, double height, String name) {
        this.length = length;
        this.height = height;
    }

    public void calculateSquare() {
        System.out.println(height * length);
    }
}
