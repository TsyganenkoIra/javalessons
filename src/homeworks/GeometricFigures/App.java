package homeworks.GeometricFigures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class App {

    public static void main(String[] args) {

        Figure romb = new Romb(3, 3);
        Figure parall = new Parallelogram(8, 10);
        Figure triangle = new Triangle(6, 4);

        ArrayList<Figure> list = new ArrayList<>();

        list.add(romb);
        list.add(parall);
        list.add(triangle);

        list.addAll(Arrays.asList(romb, parall, triangle));
//        list.addAll(List.of(romb, parall, triangle));

        for (Figure f : list) {
            f.print();
            f.calculateSquare();
        }

        Iterator<Figure> iterator = list.iterator();

        while (iterator.hasNext()) {
//            Figure next = iterator.next();
        }
    }
}
