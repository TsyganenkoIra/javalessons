package homeworks.GeometricFigures;

public class Parallelogram extends Figure {

    Parallelogram(double length, double height) {
        super(length, height, "Parallelogram");
    }

}
