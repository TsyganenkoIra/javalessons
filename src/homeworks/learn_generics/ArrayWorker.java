package homeworks.learn_generics;

public class ArrayWorker<T extends Number> {

    private T[] array;

    public void setArray(T[] array) {
        this.array = array;
    }

    public T[] getArray() {
        return this.array;
    }

    public <T extends Number> void countTheElements(T element) {

        int count = 0;

        for (int i = 0; i < array.length; i++) {
//            element.doubleValue() == array[i].doubleValue()

            /*if (element.compareTo((T) array[i]) < 0) {
                //System.out.println(element + "<" + array[i]);
                count++;
            }*/
        }

        System.out.println("Tne number of elements that are greater than " + element + " = " + count);
    }
}
