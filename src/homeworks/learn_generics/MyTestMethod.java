package homeworks.learn_generics;

public class MyTestMethod {

    public static <T extends Comparable<T>> void calcNum(T[] array, T maxElem) {
        int count = 0;

        for (T t : array) {
            if (t.compareTo(maxElem) > 0) {
                //System.out.println(t + ">" + maxElem);
                count++;
            }
        }

        System.out.println("Tne number of elements that are greater than " + maxElem + " = " + count);
    }
}
