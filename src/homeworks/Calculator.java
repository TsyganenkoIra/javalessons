package homeworks;

import java.util.Random;

public class Calculator {
    //public
    public int getRandom() {

        return new Random().nextInt(20);
    }

    public int getSum(int v1) {

        return v1 + getRandom();
    }

    public int sum() {
        return getRandomValueOne() + getRandomValueTwo();
    }

   //private
    private int getRandomValueOne() {

        return new Random().nextInt(20);
    }

    private int getRandomValueTwo() {

        return new Random().nextInt(20);
    }

    public int getSumPrivate() {

        return  getRandomValueTwo() + getRandomValueOne();
    }
    //public static
    public static int getRandomValueStatic() {

        return new Random().nextInt(20);
    }

    public static int getSumStatic(int v1) {

        return v1 + getRandomValueStatic();
    }


}
