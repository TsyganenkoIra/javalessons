package homeworks.imitation_array_list;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.sql.SQLOutput;

public class MyArray {

    private int[] arr;
    private int qty = 0;
    private BufferedReader br;
    private boolean mark = false;

    public MyArray(int size) {

        arr = new int[size];
    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public void add(int value) {

        if (value == 0) {
            return;
        }

        resize(true);

        arr[qty] = value;

        qty++;
    }

    public int getElement(int index) {
        if (index < 0 || index > arr.length - 1) {
            System.out.println("Element doesn`t exsist!");

            return 0;
        }
        return arr[index];
    }

    public void changeElement(int index, int newValue) {
        if (index < 0 | index > qty) {
            System.out.println("Element doesn`t exsist!");
            return;
        }
        arr[index] = newValue;
    }

    public void remove(int ind) {

        if (ind < 0 | ind > qty) {
            System.out.println("Element doesn`t exsist!");
            return;
        }

        for (int y = ind; y < qty - 1; y++) {
            arr[y] = arr[y + 1];
        }

        arr[qty - 1] = 0;

        qty--;

    }


    public void showAllElements() {
        for (int i = 0; i < qty; i++) {//foreach
            System.out.print(arr[i] + ", ");
        }
    }

    public void showAllElementsReverse() {
        for (int i = qty - 1; i >= 0; i--) {
            System.out.print(arr[i] + ", ");
        }
    }

    public void sort() {
        for (int i = qty - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }

    public void addAll(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            add(ar[i]);
        }
    }

    public void deleteDublicates() {

        for (int i = 0; i < qty - 1; i++) {

            for (int j = i + 1; j < qty; j++) {
                if (arr[i] == arr[j]) {
                    remove(j);
                    j--;

                }

            }

        }

    }

    public void findIndexOfElement(int i) {

        for (int j = 0; j < qty; j++)
            if (arr[j] == i) {
                System.out.println("Index of " + i + " = " + j);
                break;
            }
    }


    private void resize(boolean flag) {

        if (arr.length == 0) {
            arr = new int[5];
            return;
        }

        if (qty == arr.length) {

            int[] newArr = new int[arr.length * 2];

            for (int i = 0; i < qty; i++) {
                newArr[i] = arr[i];
            }

            arr = newArr;
        }
    }

    public void changeArrSize(int number) {

        int newSize = arr.length + number;

        if (newSize >= 0) {
            int[] newArr = new int[newSize];

            if (newSize < arr.length) {
                qty = arr.length + number;

            }

            for (int i = 0; i < qty; i++) {
                newArr[i] = arr[i];

            }

            arr = newArr;
        }
    }

    public int getSize() {
        return arr.length;
    }
}
