package homeworks.imitation_array_list;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    public static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        MyArray array = new MyArray(10);

        boolean mark = true;

        do {
            System.out.println("Choose action, please:" + "\n1: Add element");
            System.out.println("2: Change element by index");
            System.out.println("3: Delete element by index");
            System.out.println("4: Change size of array");
            System.out.println("5: Show all elements");
            System.out.println("6: Show all elements in reverse");
            System.out.println("7: Sort elements");
            System.out.println("8: Add array to array");
            System.out.println("9: Delete dublicates");
            System.out.println("10: Find index of element");
            System.out.println("0: Exit");

            String action = READER.readLine().toString();

            switch (action) {
                case "0":
                    mark = false;
                    break;

                case "1":
                    try {
                        int el = getInfo("Type element, please:");
                        array.add(el);
                    } catch (Exception e) {
                        System.out.println("Unsuitable format. Please, try again.");
                    }
                    break;

                case "2":
                    try {
                        int index =  getInfo("Type index of element to change, please:");

                        System.out.println("Type new value, please:");
                        int newValue = Integer.parseInt(READER.readLine());
                        array.changeElement(index, newValue);

                    } catch (Exception e) {
                        System.out.println("Element does not exist OR Unsuitable format of index/new value. Please, try again.");
                    }
                    break;

                case "3":
                    System.out.println("Type index of element to delete, please:");
                    try {
                        int index = Integer.parseInt(READER.readLine());
                        array.remove(index);

                    } catch (Exception e) {
                        System.out.println("Element does not exist OR Unsuitable format of index. Please, try again.");
                    }
                    break;

                case "4":
                    System.out.println("Type the number to change array, please:");
                    try {
                        int newSize = Integer.parseInt(READER.readLine());
                        array.changeArrSize(newSize);

                    } catch (Exception e) {
                        System.out.println("Unsuitable format of new size. Please, try again.");
                    }
                    break;

                case "5":
                    array.showAllElements();
                    break;

                case "6":
                    array.showAllElementsReverse();
                    break;

                case "7":
                    array.sort();
                    break;

                case "8":
                    int[] arrayToAdd = {5, 4, 3, 2, 1};
                    array.addAll(arrayToAdd);
                    break;

                case "9":
                    array.deleteDublicates();
                    break;

                case "10":
                    System.out.println("Type element to find, please:");
                    try {
                        int element = Integer.parseInt(READER.readLine());
                        array.findIndexOfElement(element);

                    } catch (Exception e) {
                        System.out.println("Element does not exist.");
                    }
                    break;

                default:
                    System.out.println("Unknown action");
                    break;


            }
        } while (mark);
    }

    private static int getInfo(String message) throws IOException {

        System.out.println(message);

        return Integer.parseInt(READER.readLine());
    }
}
