package homeworks.exceptions;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MyException extends IOException {

    public MyException(String text, String directoryForExceptionFile) throws IOException {

        Path path = Paths.get(directoryForExceptionFile);
        Files.createFile(path);

        FileWriter writer = new FileWriter(path.toString(), false);
        writer.write(text);
        writer.close();

    }

}
