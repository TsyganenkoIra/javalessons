package homeworks.regex;

import homeworks.collections.FindMaxGap;

public class RegexApp {
    public static void main(String[] args) {

    System.out.println(LearnRegex.checkRegexLettersPlusTwoNumbers("aaaaaaaaaaaaa11"));

        System.out.println(LearnRegex.checkRegexTwoNumbersPlusLetters("11"));

        System.out.println(LearnRegex.checkRegexTwoOrFourNumbersPlusTwoOrFourLetters("11aaaa"));

        System.out.println(LearnRegex.checkRegexTwoOrFourLettersPlusTwoOrFourNumbers("aa11"));

        System.out.println(LearnRegex.checkRegexNameAndRegister("Ivan Petrov"));
        System.out.println(LearnRegex.checkRegexNameAndRegister("ivan Petrov"));

        System.out.println(LearnRegex.calculateSumOfNumbersInString("25aa 5aa 8a2"));



    }
}
