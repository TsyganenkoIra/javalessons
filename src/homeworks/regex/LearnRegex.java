package homeworks.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LearnRegex {

    public static boolean checkRegexLettersPlusTwoNumbers(String text){

       String regex = "^(?i)[a-z]*\\d{2}";
       return Pattern.matches(regex, text);
    }

    public static boolean checkRegexTwoNumbersPlusLetters(String text){
        String regex = "^(?i)\\d{2}[a-z]*";
        return Pattern.matches(regex, text);
    }

    public static boolean checkRegexTwoOrFourNumbersPlusTwoOrFourLetters(String text){
        String regex = "^(?i)(\\d{2}|\\d{4})([a-z]{2}|[a-z]{4})";
        return Pattern.matches(regex, text);
    }

    public static boolean checkRegexTwoOrFourLettersPlusTwoOrFourNumbers(String text){
        String regex = "^(?i)([a-z]{2}|[a-z]{4})(\\d{2}|\\d{4})";
        return Pattern.matches(regex, text);
    }

    public static boolean checkRegexNameAndRegister(String text){
        String regex = "[A-Z][a-z]+\\s[A-Z][a-z]+";
        return Pattern.matches(regex, text);
    }

    public static int calculateSumOfNumbersInString(String text){

        int sum = 0;

        Pattern pattern = Pattern.compile("\\d+");

        Matcher matcher = pattern.matcher(text);

        while (matcher.find()){
            sum = sum + Integer.valueOf(matcher.group());
        }

        return sum;
    }
}
