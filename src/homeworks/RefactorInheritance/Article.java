package homeworks.RefactorInheritance;

public class Article extends Reference {
    private int startPage;
    private int endPage;

    public Article(String author1, String author2, String author3, String author4, String name, Boolean isUpdated, int year, int volume, int startPage, int endPage) {
        super(author1, author2, author3, author4, name, isUpdated, year, volume);
        this.startPage = startPage;
        this.endPage = endPage;
    }

    @Override
    public void processing() {
        System.out.println("Do something with article");
    }
}
