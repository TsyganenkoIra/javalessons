package homeworks.RefactorInheritance;

public class Book extends Reference {
    private int countOfpages;

    public Book(String author1, String author2, String author3, String author4, String name, Boolean isUpdated, int year, int volume, int countOfPage) {
        super(author1, author2, author3, author4, name, isUpdated, year, volume);
        this.countOfpages = countOfPage;
    }

    public int getCountOfpages() {
        return countOfpages;
    }

    public void setCountOfpages(int countOfpages) {
        this.countOfpages = countOfpages;
    }

    @Override
    public void processing() {
        System.out.println("Do something with book ");
    }
}
