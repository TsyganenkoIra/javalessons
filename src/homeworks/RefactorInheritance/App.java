package homeworks.RefactorInheritance;

public class App {

    public static void main(String[] args) {


        Book book = new Book("a1", "a2", "a3", "a4",
                "Book1", true, 2012, 350, 900);

        book.getCountOfpages();

        Reference article = new Article("au1", "au2", "au3", "au4",
                "Article1", true, 2012, 350, 5, 7);

        book.processing();
        article.processing();
    }
}
