package homeworks.RefactorInheritance;

public abstract class Reference implements Procesable {
    private String author1;
    private String author2;
    private String author3;
    private String author4;
    private String name;
    private Boolean isUpdated;
    private int year;
    private int volume;

    public Reference(String author1, String author2, String author3, String author4,
                     String name, Boolean isUpdated, int year, int volume) {
        this.author1 = author1;
        this.author2 = author2;
        this.author3 = author3;
        this.author4 = author4;
        this.name = name;
        this.isUpdated = isUpdated;
        this.year = year;
        this.volume = volume;
    }
}
