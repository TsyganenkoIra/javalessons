package parametrized_test;

import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ParametersJunit {

    @Parameterized.Parameter(value = 1)
    public int value1;

    @Parameterized.Parameter
    public int value2;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{1, 7}, {2, 5}};
        return Arrays.asList(data);
    }

    @Test
    public void useJunitParamsLib() {
        Assert.assertEquals(value1 + value2, new Calculator().sum(value1, value2));
    }
}
