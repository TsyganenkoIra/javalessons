package generics;

import homeworks.learn_generics.ArrayWorker;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class ArrayWorkerTest {

    @Rule
    public final SystemOutRule outRule = new SystemOutRule();

    @Test
    public void shouldSetArray(){
        ArrayWorker worker = new ArrayWorker();
        Integer[] array = {1, 2, 3};
        worker.setArray(array);

        Assert.assertEquals(array, worker.getArray());

    }


    @Test
    public void shouldGetArray(){
        ArrayWorker worker = new ArrayWorker();
        Integer[] array = {1, 2, 3};
        worker.<Integer>setArray(array);
        Integer[] workArray = (Integer[]) worker.getArray();

        Assert.assertEquals(array, workArray);

    }

    @Test
    public void shouldcountTheElements(){
        ArrayWorker worker = new ArrayWorker();
        Integer[] array = {1, 2, 3};
        worker.<Integer>setArray(array);

        worker.countTheElements(1);

        Assert.assertTrue(outRule.getLog().contains("Tne number of elements that are greater than 1 = 2"));
    }
}
