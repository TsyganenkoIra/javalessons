package generics;

import homeworks.learn_generics.MyTestMethod;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class MyTestMethodTest {
    @Rule
    public final SystemOutRule outRule = new SystemOutRule();

    @Test
    public void ShouldCalcNumOfElemGreaterThen(){
        MyTestMethod.<Integer>calcNum(new Integer[]{5,6,7,8,9}, 5);
        Assert.assertTrue(outRule.getLog().contains("Tne number of elements that are greater than 5 = 4"));
    }
}
