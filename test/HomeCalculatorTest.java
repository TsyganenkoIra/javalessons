import homeworks.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


//@RunWith(MockitoJUnitRunner.class)

@RunWith(PowerMockRunner.class)
@PrepareForTest({Calculator.class})
public class HomeCalculatorTest {

    @Mock
    private Calculator calculator;

    @Test
    public void shouldGetSumPrivate() throws Exception {

        Calculator calculator = new Calculator();

        Calculator instance = PowerMockito.spy(calculator);

        String methodToTest1 = "getRandomValueOne";

        String methodToTest2 = "getRandomValueTwo";

        PowerMockito.doReturn(10).when(instance, methodToTest1);

        PowerMockito.doReturn(5).when(instance, methodToTest2);

        Assert.assertTrue(instance.getSumPrivate() == 15);

    }

    @Test
    public void shouldGetSumStatic() {

        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomValueStatic()).thenReturn(10);

        Mockito.when(Calculator.getSumStatic(5)).thenCallRealMethod();

        Assert.assertEquals(15, Calculator.getSumStatic(5));
    }

    @Test
    public void shouldGetSum(){

        Mockito.when(calculator.getRandom()).thenReturn(10);
        Mockito.when(calculator.getSum(5)).thenCallRealMethod();

        Assert.assertEquals(15, calculator.getSum(5));
    }


}
