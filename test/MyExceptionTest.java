import homeworks.exceptions.MyException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyExceptionTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final SystemOutRule outRule = new SystemOutRule();

    @Test
    public void shoudThrowMyException() throws IOException {

        File newFolder = folder.newFolder("FolderForExceptionFile");

        String pathToFile = newFolder.toPath().toString() + "/ExceptionFile.pdf";

        MyException myException = new MyException("Test text", pathToFile);

        File file = new File(newFolder + "/ExceptionFile.pdf");

        BufferedReader br = new BufferedReader (new FileReader(file)) ;
        String textFromFile = br.readLine().toString();

        Assert.assertEquals(textFromFile, "Test text");
    }
}
