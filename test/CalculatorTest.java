import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lessons.for_test.Calculator;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLData;
import java.sql.SQLDataException;

@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final SystemOutRule outRule = new SystemOutRule().enableLog();

    @Before
    public void runBeforeEachTest() {
//        System.out.println("Run before each test");
    }

    @BeforeClass
    public static void beforeAllTests() {
//        System.out.println("Before all tests");
    }

    @Test
    public void shouldReturnSumValue() {

        Calculator calculator = new Calculator();

        Assert.assertEquals("Should return 10", 10, calculator.sum(7, 3));

//        Assert.assertTrue(true);
    }

    @Test
    public void shouldReturnSumValue1() {
        Calculator calculator = new Calculator();

        Assert.assertEquals("Should return 10", 10, calculator.sum(7, 3));
    }

    @After
    public void runAfterEachTest() {
//        System.out.println("Run after each test");
    }

    @AfterClass
    public static void afterAllTests() {
//        System.out.println("After all tests");
    }

    public int getMessage() {
        try {
            return 1;
        } catch (Exception e) {
            e = new NullPointerException();
            throw e;
        } finally {
            return 2;
        }

    }


    /*
    * A() {
    * B();
    * }
    *
    * B() {
    *
    * }
    * */

    @Test
    public void useTemporaryFolder() throws IOException {
//        File file = folder.newFile("Test.txt");

//        File run = folder.newFolder("run");

        File file1 = folder.newFolder("yet", "my");

        Files.createFile(Paths.get(file1.getPath(), "Ira.txt"));

        System.out.println();

    }

    @Test
    public void useSystemOutRule() {
        System.out.println("Hello world");

        Assert.assertTrue(outRule.getLog().contains("Hello world"));

    }

    @Test
    @Parameters({"5|6", "-7|9"})
    public void useJunitParamsLib(int value1, int value2) {
        Assert.assertEquals(value1 + value2, new Calculator().sum(value1, value2));
    }



}
