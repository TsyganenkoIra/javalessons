package mockito;

import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Calculator.class)
public class UsePowerMockito {

    @Test
    public void shouldReturnSumValue() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getSomeValue()).thenReturn(6);

        Assert.assertEquals(12, new Calculator().newSum(6));
    }

    @Test
    public void shouldReturnSumValueFinal() {
        Calculator mock = Mockito.mock(Calculator.class);

    //    Mockito.when(mock.getRandomValue()).thenReturn(6);

        Mockito.when(mock.newSum(6)).thenCallRealMethod();

        Assert.assertEquals(12, new Calculator().newSum(6));
    }

    @Test
    public void shouldMockFinalMethod() {
        Calculator calculator = PowerMockito.mock(Calculator.class);

        PowerMockito.doReturn(5).when(calculator).getValueFinal();

        Assert.assertTrue(calculator.getValueFinal() == 5);
    }
}
