package mockito;

import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UseMockito {

    @Mock
    private Calculator mock;

    @Spy
    private Calculator spy;

    @Test
    public void shouldReturnSumValue() {

    //    Mockito.when(mock.getRandomValue()).thenReturn(5);

        Mockito.when(mock.newSum(8)).thenCallRealMethod();

        Assert.assertEquals(13, mock.newSum(8));
    }

    @Test
    public void shouldReturnSumValue1() {

 //       Mockito.when(spy.getRandomValue()).thenReturn(5);

        Assert.assertEquals(13, spy.newSum(8));
    }
}
