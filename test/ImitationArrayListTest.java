import homeworks.imitation_array_list.MyArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class ImitationArrayListTest {

    private MyArray testArray = new MyArray(0);


    @Rule
    public final SystemOutRule outRule = new SystemOutRule().enableLog();

    @Before
    public void runBeforeEachTest() {
        testArray.setArr(new int[1]);
    }

    @Test
    public void shouldAddElement() {

        testArray.add(10);

        Assert.assertEquals("Should return element with index 0: 10",
                10, testArray.getElement(0));
    }

    @Test
    public void shouldReturnElementByIndex() {

        int index;

        given:

        testArray.add(10);

        testArray.add(20);

        When:
        index = 1;

        then:
        {
            Assert.assertEquals("Should return element with index 1: 20",
                    20, testArray.getElement(index));
        }

        when:
        index = 5;

        then:
        {
            Assert.assertEquals("Should return 0",
                    0, testArray.getElement(index));

            Assert.assertTrue(outRule.getLog().contains("Element doesn`t exsist!"));
        }

    }

    @Test
    public void shouldChangeElementByIndexToNew() {

        testArray.add(5);

        testArray.changeElement(0, 55);

        Assert.assertEquals("Should change element with index 0 to new value - 55",
                55, testArray.getElement(0));
    }


    @Test
    public void shouldRemoveElementByIndex() {

        testArray.add(5);

        testArray.remove(0);

        testArray.getElement(0);

        Assert.assertTrue(outRule.getLog().contains("Element doesn`t exsist!"));
    }

    @Test
    public void shouldShowAllElements() {

        testArray.add(1);
        testArray.add(2);
        testArray.add(3);

        testArray.showAllElements();
        Assert.assertTrue(outRule.getLog().contains("1, 2, 3, "));
    }

    @Test
    public void shouldShowAllElementsReverse() {

        testArray.add(1);
        testArray.add(2);
        testArray.add(3);

        testArray.showAllElementsReverse();
        Assert.assertTrue(outRule.getLog().contains("3, 2, 1, "));
    }

    @Test
    public void shouldSortElements() {

        given:
        testArray.add(5);
        testArray.add(1);
        testArray.add(3);

        when:
        testArray.sort();

        then:
        {

            int[] arrayToCompare = {1, 3, 5, 0};

            Assert.assertArrayEquals(arrayToCompare, testArray.getArr());
        }

        when:

        testArray.add(7);
        testArray.sort();

        then:
        {

            int[] arrayToCompare = {1, 3, 5, 7};

            Assert.assertArrayEquals(arrayToCompare, testArray.getArr());
        }
    }

    @Test
    public void shouldAddArray() {

        testArray.add(1);
        int[] arrayToAdd = {2, 3, 4, 5};
        testArray.addAll(arrayToAdd);

        int size = testArray.getSize();
        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = testArray.getElement(i);
        }

        int[] arrayToCompare = {1, 2, 3, 4, 5};

        Assert.assertArrayEquals(arrayToCompare, array);

    }

    @Test
    public void shoulDeleteDublicates() {

        testArray.addAll(new int[]{2, 2});
        testArray.deleteDublicates();

        int size = testArray.getSize();

        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = testArray.getElement(i);
        }

        int[] arrayToCompare = new int[size];

        arrayToCompare[0] = 2;

        Assert.assertArrayEquals(arrayToCompare, array);
    }

    @Test
    public void shoudFindIndexOfElementDyValue() {
        testArray.addAll(new int[]{1, 2, 3});
        testArray.findIndexOfElement(3);
        Assert.assertTrue(outRule.getLog().contains("Index of 3 = 2"));
    }

    @Test
    public void shouldChangeArraySize() {

        testArray.changeArrSize(10);
        int size = testArray.getSize();

        Assert.assertEquals(10, size);
    }

}
