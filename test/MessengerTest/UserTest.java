package MessengerTest;

import homeworks.messenger.Message;
import homeworks.messenger.Status;
import homeworks.messenger.User;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import java.nio.file.Path;
import java.time.LocalDate;
import java.time.Period;


public class UserTest {

    private User mock;
    private Message messageMock;
    private Path path;

    @Test
    public void shouldAddSMS() {
        LocalDate date = LocalDate.of(2000, 10, 10);

        mock = new User("User", date);

        mock.addSMS(messageMock);

        Assert.assertEquals(1, mock.getMessages().size());
    }

    @Test
    public void shouldGetName() {
        LocalDate date = LocalDate.of(2000, 10, 10);
        mock = new User("User", date);

        Assert.assertEquals("User", mock.getName());
    }

    @Test
    public void shouldGetAge() {
        LocalDate date = LocalDate.of(2000, 10, 10);
        mock = new User("User", date);

        Assert.assertEquals(Period.between(date, LocalDate.now()).getYears(), mock.getAge());
    }

    @Test
    public void shouldGetMessages() {
        LocalDate date = LocalDate.of(2000, 10, 10);
        mock = new User("User", date);
        mock.addSMS(messageMock);

        Assert.assertEquals(1, mock.getMessages().size());
    }


}
