package MessengerTest;

import homeworks.messenger.Messenger;
import homeworks.messenger.User;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

public class MessengerTest {

    @Mock
    private Messenger messengerMock;
    private User user;

    @Test
    public void shouldGetUsers() {
        messengerMock = new Messenger();

        messengerMock.registerUser(user);

        Assert.assertEquals(1, messengerMock.getUsers().size());
    }

    @Test
    public void shouldSetUsers() {
        messengerMock = new Messenger();
        messengerMock.registerUser(user);

        Assert.assertEquals(1, messengerMock.getUsers().size());
    }
}
