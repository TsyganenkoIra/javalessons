import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import homeworks.file_manager.FileManager;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileManagerTest {

    @Rule
    public final SystemOutRule outRule = new SystemOutRule().enableLog();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    private final FileManager fileManager = new FileManager();

    @Test
    public void shouldCreateFile() throws IOException {
        File newFolder = folder.newFolder();

        Path path = Paths.get(newFolder.toPath().toString() + "/testFile.txt");

        fileManager.createFile(path);

        Assert.assertTrue(Files.exists(path));
    }

    @Test
    public void shouldCreateDirectory() throws IOException {
        File newFolder = folder.newFolder();

        Path path = Paths.get(newFolder.toPath().toString() + "/TestDir");

        fileManager.createDirectory(path);

        Assert.assertTrue(Files.exists(path));
    }

    @Test
    public void shouldConvertToPDF() throws IOException {
        File newFolder = folder.newFolder();

        Path path = Paths.get(newFolder + "/testFile.txt");

        fileManager.createFile(path);
        Assert.assertTrue(Files.exists(path));


        try (FileWriter writer = new FileWriter(newFolder + "/testFile.txt", false)) {
            String text = "Text to check";
            writer.write(text);
        }

        fileManager.convertToPDF(path);
        Path pdfPath = Paths.get(newFolder + "/testFile.txt.pdf");

        Assert.assertTrue(Files.exists(pdfPath));

        PdfReader reader = new PdfReader(newFolder + "/testFile.txt.pdf");

        StringBuffer sb = new StringBuffer();

        PdfReaderContentParser parser = new PdfReaderContentParser(reader);

        TextExtractionStrategy strategy;

        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
            sb.append(strategy.getResultantText());
        }

        reader.close();
        String textFromPDF = sb.toString();
        Assert.assertEquals("Text to check", textFromPDF);
    }

    @Test
    public void shouldCopyFilesToOtherDirectory() throws IOException {
        File newFolder = folder.newFolder();

        Path path = Paths.get(newFolder + "/testFile.txt");
        fileManager.createFile(path);

        Path pathFrom = Paths.get(newFolder.toPath().toString());

        Path pathTo = Paths.get(newFolder + "/checkCopyFiles");

        fileManager.copyFilesToOtherDirectory(pathFrom, pathTo);

        Path pathOfCopiedFile = Paths.get(pathTo + "/testFile.txt");

        Assert.assertTrue(Files.exists(pathOfCopiedFile));

    }

    @Test
    public void shouldCheckPathExistence() throws IOException {
        File newFolder = folder.newFolder();
        Path pathToCheck = Paths.get(newFolder.toPath().toString());
        Assert.assertTrue(Files.exists(pathToCheck));
    }

    @Test
    public void shouldDeleteFile() throws IOException {
        File newFolder = folder.newFolder();
        Path path = Paths.get(newFolder + "/testFile.txt");

        fileManager.createFile(path);
        Assert.assertTrue(Files.exists(path));

        fileManager.deleteFile(path);
        Assert.assertFalse(Files.exists(path));
    }

    @Test
    public void shouldDeleteDirectory() throws IOException {
        File newFolder = folder.newFolder();
        Path path = Paths.get(newFolder + "/TestDir");

        fileManager.createDirectory(path);
        Assert.assertTrue(Files.exists(path));

        fileManager.deleteDirectory(path);
        Assert.assertFalse(Files.exists(path));
    }

    @Test
    public void shouldLookThroughDirectory() throws IOException {
        File newFolder = folder.newFolder();
        Path path = Paths.get(newFolder + "/testFile.txt");
        fileManager.createFile(path);

        fileManager.lookThroughDirectory(path.getParent());

        Assert.assertTrue(outRule.getLog().contains("testFile.txt"));
    }

    @Test
    public void shouldRenameFile() throws IOException {
        File newFolder = folder.newFolder();

        Path path = Paths.get(newFolder + "/testFile.txt");
        fileManager.createFile(path);

        fileManager.renameFile(String.valueOf(path), "renamedFile.txt");
        Path newPath = Paths.get(newFolder + "/renamedFile.txt");

        Assert.assertEquals(false, Files.exists(path));
        Assert.assertEquals(true, Files.exists(newPath));
    }

    @Test
    public void shouldRenameDirectory() throws IOException {
        File newFolder = folder.newFolder();

        Path path = Paths.get(newFolder + "/TestDir");
        fileManager.createDirectory(path);

        Path newPath = Paths.get(newFolder+"/RenamedDir" );

        fileManager.renameDirectory(path, newPath);

        Assert.assertFalse(Files.exists(path));
        Assert.assertTrue(Files.exists(newPath));
    }

}
